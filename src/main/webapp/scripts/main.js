const GRAPH_SIZE = 300;
const R_SIZE = 100;
let dots = [];
let x, y, r;
let graph = document.getElementById("svg-figure");

$("#svg-figure").bind("click", addDot);

$(document).ready(function() {
    const getSelectedCheckbox = function(widget) {
        let result = ""
        widget.inputs.each(function(index, input) {
            if (input.getAttribute('checked') === 'checked') {
                result = input.getAttribute('value');
                return false;
            }
        });
        return result;
    }

    x = getSelectedCheckbox(PF('x-widget'));
    y = PF('y-widget').jq[0].value
    r = getSelectedCheckbox(PF('r-widget'));
    updateArea();

    let rows = $('#history tr.ui-widget-content');
    rows.each(function(i, row) {
        createDot(row.children[0].textContent, row.children[1].textContent);
    });
});

function updateX(xCheckbox) {
    if (xCheckbox.getAttribute('aria-checked') === 'true') {
        let input = $(xCheckbox);
        let checkbox = input.parent().next(".ui-chkbox-box");
        let widget = PF('x-widget');

        widget.uncheckAll();
        widget.check(input, checkbox);

        x = parseFloat(input.attr('value'));
        updateArea();
    }
    else {
        x = "";
    }
}

function updateY(yInput) {
    y = parseFloat(yInput.value);
    updateArea();
}

function updateR(rCheckbox) {
    if (rCheckbox.getAttribute('aria-checked') === 'true') {
        let input = $(rCheckbox);
        let checkbox = input.parent().next(".ui-chkbox-box");
        let widget = PF('r-widget');

        widget.uncheckAll();
        widget.check(input, checkbox);

        r = parseFloat(input.attr('value'));
        updateArea();
    }
    else {
        r = "";
    }
}

function createDot(x, y) {
    let cx = x * R_SIZE / r + GRAPH_SIZE / 2,
        cy = GRAPH_SIZE / 2 - y * R_SIZE / r,
        fill = checkCollision(x, y) ? "green" : "red";

    let newDot = document.createElementNS("http://www.w3.org/2000/svg", "circle");
    newDot.setAttribute('cx', cx);
    newDot.setAttribute('cy', cy);
    newDot.setAttribute('r', '4');
    newDot.setAttribute('fill', fill);
    newDot.setAttribute('stroke', 'white');
    newDot.setAttribute('stroke-width', '1');
    newDot.setAttribute('class', 'history-dot');

    graph.insertBefore(newDot, document.getElementById('preview-dot'));
    dots.push([newDot, parseFloat(x), parseFloat(y)]);

    return newDot;
}

function drawDot() {
    createDot(x, y);
}

function updateArea() {
    console.log(dots);

    if (r > 0) {
        $("#preview-dot").attr({
            cx: x * R_SIZE / r + GRAPH_SIZE / 2,
            cy: GRAPH_SIZE / 2 - y * R_SIZE / r,
            visibility: "visible"
        });

        $(".r-text").text(r);
        $(".neg-r-text").text(-r);
        $(".r-2-text").text(r / 2);
        $(".neg-r-2-text").text(-r / 2);

        for (let dot of dots) {
            dot[0].setAttribute('cx', dot[1] * R_SIZE / r + GRAPH_SIZE / 2);
            dot[0].setAttribute('cy', GRAPH_SIZE / 2 - dot[2] * R_SIZE / r);
            dot[0].setAttribute('fill', checkCollision(dot[1], dot[2]) ? 'green' : 'red');
        }
    }
}

function checkCollision(dotX, dotY) {
    return (dotX <= Number.EPSILON && dotY >= -Number.EPSILON && dotY <= dotX + r/2 + Number.EPSILON) ||
        (dotX <= Number.EPSILON && dotY <= Number.EPSILON && dotX * dotX + dotY * dotY <= r * r + Number.EPSILON) ||
        (dotX >= -Number.EPSILON && dotY <= Number.EPSILON && dotX <= r/2 + Number.EPSILON && dotY >= -r - Number.EPSILON)
}

function addDot(event) {
    let mouseX = event.clientX - this.getBoundingClientRect().left;
    let mouseY = event.clientY - this.getBoundingClientRect().top;

    mouseX -= 150;
    mouseY -= 150;
    mouseX = mouseX / R_SIZE * r;
    mouseY = - mouseY / R_SIZE * r;

    if (!validateNumber(mouseX, -5, 1)) {
        PF('growlWidget').renderMessage({
            summary: 'Ошибка проверки X',
            detail: 'X должен быть от -5 до 1',
            severity: 'error'
        });
    }
    else if (!validateNumber(mouseY, -5, 3)) {
        PF('growlWidget').renderMessage({
            summary: 'Ошибка проверки Y',
            detail: 'Y должен быть от -5 до 3',
            severity: 'error'
        });
    }
    else if (!validateNumber(r, 1, 3)) {
        PF('growlWidget').renderMessage({
            summary: 'Ошибка проверки R',
            detail: 'R должен быть от 1 до 3',
            severity: 'error'
        });
    }
    else {
        createDot(mouseX, mouseY);

        saveShot([{name: 'x-input', value: mouseX},
            {name: 'y-input', value: mouseY},
            {name: 'r-input', value: r}]);
    }
}

function validateNumber(value, min, max) {
    return !(value === "" ||
            isNaN(value) ||
            isNaN(Number.parseFloat(value)) ||
            value < min ||
            value > max);
}

function clearDots() {
    const dotElements = document.querySelectorAll('.history-dot');
    dotElements.forEach(dot => {
        dot.remove();
    });

    dots = [];
}