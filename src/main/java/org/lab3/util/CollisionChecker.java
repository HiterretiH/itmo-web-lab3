package org.lab3.util;

import org.lab3.model.Shot;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class CollisionChecker {
    public static boolean checkCollision(Shot shot) {
        BigDecimal x = new BigDecimal(shot.getX());
        BigDecimal y = new BigDecimal(shot.getY());
        BigDecimal r = new BigDecimal(shot.getR());

        return (x.compareTo(BigDecimal.ZERO) <= 0 && y.compareTo(BigDecimal.ZERO) >= 0 &&
                y.compareTo(x.add(r.multiply(BigDecimal.valueOf(0.5)))) <= 0) ||

                (x.compareTo(BigDecimal.ZERO) <= 0 && y.compareTo(BigDecimal.ZERO) <= 0 &&
                x.multiply(x).add(y.multiply(y)).compareTo(r.multiply(r)) <= 0) ||

                (x.compareTo(BigDecimal.ZERO) >= 0 && y.compareTo(BigDecimal.ZERO) <= 0 &&
                x.compareTo(r.multiply(BigDecimal.valueOf(0.5))) <= 0 && y.compareTo(r.negate()) >= 0);
    }
}
