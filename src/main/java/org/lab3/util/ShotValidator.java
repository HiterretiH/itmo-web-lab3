package org.lab3.util;

import org.lab3.model.Shot;

import java.math.BigDecimal;

public class ShotValidator {
    public static BigDecimal minus5 = new BigDecimal(-5);
    public static BigDecimal three = new BigDecimal(3);

    public static boolean validate(Shot shot) {
        return validateNumber(shot.getX(), minus5, BigDecimal.ONE) &&
                validateNumber(shot.getY(), minus5, three) &&
                validateNumber(shot.getR(), BigDecimal.ONE, three);
    }

    public static boolean validateNumber(String s, BigDecimal min, BigDecimal max) {
        try {
            BigDecimal number = new BigDecimal(s);
            return number.compareTo(min) >= 0 && number.compareTo(max) <= 0;
        }
        catch (NullPointerException | NumberFormatException e) {
            return false;
        }
    }
}
