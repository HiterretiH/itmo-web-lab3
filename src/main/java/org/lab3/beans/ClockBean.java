package org.lab3.beans;

import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Named;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;

@Named("clock")
@ApplicationScoped
public class ClockBean implements Serializable {
    private final SimpleDateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy");
    private final SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm:ss");

    public String getDate() {
        return dateFormat.format(new Date());
    }

    public String getTime() {
        return timeFormat.format(new Date());
    }
}
