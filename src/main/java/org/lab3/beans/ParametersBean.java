package org.lab3.beans;

import jakarta.annotation.PostConstruct;
import jakarta.enterprise.context.SessionScoped;
import jakarta.faces.application.FacesMessage;
import jakarta.faces.context.FacesContext;
import jakarta.inject.Inject;
import jakarta.inject.Named;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.extern.jbosslog.JBossLog;
import org.lab3.ShotManager;
import org.lab3.model.Shot;
import org.lab3.util.CollisionChecker;
import org.lab3.util.ShotValidator;

import java.io.Serializable;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Map;

@Named("parameters")
@SessionScoped
@NoArgsConstructor
@JBossLog
public class ParametersBean implements Serializable {
    @Inject
    private ShotManager manager;
    @Getter @Setter
    private String[] xSelected;
    @Getter @Setter
    private String[] rSelected;
    @Getter
    private final List<String> xValues = List.of("-5", "-4", "-3", "-2", "-1", "0", "1");
    @Getter
    private final List<String> rValues = List.of("1", "1.5", "2", "2.5", "3");
    @Getter @Setter
    private String y;
    @Getter @Setter
    private List<Shot> shotList;

    @PostConstruct
    public void init() {
        shotList = manager.loadShots();
    }

    public void persistShot(Shot shot) {
        if (!ShotValidator.validate(shot)) {
            log.error("Shot validation error for " + shot);
            FacesContext.getCurrentInstance().
                    addMessage(null,
                            new FacesMessage(
                                    FacesMessage.SEVERITY_ERROR, "Ошибка проверки значений", "Ошибка проверки значений"));
            return;
        }

        shot.setHit(CollisionChecker.checkCollision(shot));
        shot.setTime(LocalTime.now().format(DateTimeFormatter.ofPattern("HH:mm:ss")));

        manager.saveShot(shot);
        shotList.add(shot);
    }

    public void deleteShots() {
        manager.deleteShots();
        shotList.clear();
    }

    public void saveShot() {
        Shot newShot = new Shot();
        y = y.replace(',', '.');

        newShot.setX(getX());
        newShot.setY(y);
        newShot.setR(getR());

        persistShot(newShot);
    }

    public void savePlotShot() {
        Map<String, String> requestParameters = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();

        Shot newShot = new Shot();
        newShot.setX(requestParameters.get("x-input"));
        newShot.setY(requestParameters.get("y-input"));
        newShot.setR(requestParameters.get("r-input"));

        persistShot(newShot);
    }

    public String getX() {
        if (xSelected != null && xSelected.length > 0) return xSelected[0];
        else return "";
    }

    public String getR() {
        if (rSelected != null && rSelected.length > 0) return rSelected[0];
        else return "";
    }
}
