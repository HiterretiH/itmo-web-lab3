package org.lab3;

import jakarta.enterprise.context.ApplicationScoped;
import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;
import jakarta.transaction.Transactional;
import lombok.extern.jbosslog.JBossLog;
import org.lab3.model.Shot;

import java.io.Serializable;
import java.util.List;

@ApplicationScoped
@JBossLog
public class ShotManager implements Serializable {
    @PersistenceContext
    private EntityManager entityManager;

    @Transactional
    public void saveShot(Shot shot) {
        try {
            entityManager.getTransaction().begin();
            entityManager.merge(shot);
            entityManager.getTransaction().commit();
        } catch (Exception e) {
            if (entityManager.getTransaction().isActive()) {
                entityManager.getTransaction().rollback();
                }
            log.error(e.getMessage());
        }
    }

    public List<Shot> loadShots() {
        return entityManager.createQuery("SELECT s FROM Shot s", Shot.class).getResultList();
    }

    @Transactional
    public void deleteShots() {
        try {
            entityManager.getTransaction().begin();
            entityManager.createQuery("DELETE FROM Shot").executeUpdate();
            entityManager.getTransaction().commit();
        } catch (Exception e) {
            if (entityManager.getTransaction().isActive()) {
                entityManager.getTransaction().rollback();
            }
            log.error(e.getMessage());
        }
    }
}
