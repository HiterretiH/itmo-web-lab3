package org.lab3.model;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;

@Entity
@Getter
@Setter
@ToString
public class Shot implements Serializable {
    @Id
    @GeneratedValue
    private long id;
    private String x;
    private String y;
    private String r;
    private boolean hit;
    private String time;

    public Shot() {}
}
