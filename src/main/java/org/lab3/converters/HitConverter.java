package org.lab3.converters;

import jakarta.faces.component.UIComponent;
import jakarta.faces.context.FacesContext;
import jakarta.faces.convert.Converter;
import jakarta.faces.convert.FacesConverter;

@FacesConverter("hitConverter")
public class HitConverter implements Converter<Boolean> {
    public static String hitString = "Попадание";
    public static String missString = "Промах";

    @Override
    public Boolean getAsObject(FacesContext facesContext, UIComponent uiComponent, String s) {
        return s.equals(hitString);
    }

    @Override
    public String getAsString(FacesContext facesContext, UIComponent uiComponent, Boolean aBoolean) {
        return aBoolean ? hitString : missString;
    }
}
